package lt.juodamorka.darkskyghost.dickbutt;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

class NotesDbHelper extends SQLiteOpenHelper {
    
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "notes.db";
    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + NoteEntry.TABLE_NAME + " (" + NoteEntry._ID + " INTEGER PRIMARY KEY," + NoteEntry.COLUMN_NAME_TITLE + " TEXT," + NoteEntry.COLUMN_NAME_CONTENT + " TEXT)";
    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + NoteEntry.TABLE_NAME;
    
    NotesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }
    
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    static class NoteEntry implements BaseColumns {
        static final String TABLE_NAME = "notes";
        static final String COLUMN_NAME_TITLE = "title";
        static final String COLUMN_NAME_CONTENT = "content";
    }
}
