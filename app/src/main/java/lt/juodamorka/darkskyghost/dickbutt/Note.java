package lt.juodamorka.darkskyghost.dickbutt;

public class Note {
    
    private final String id;
    private final String title;
    private final String content;
    
    public Note(String id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;
    
    }
    
    @Override
    public String toString() {
        return content;
    }
    
    public String getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getContent() {
        return content;
    }
}
