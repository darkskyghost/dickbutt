package lt.juodamorka.darkskyghost.dickbutt;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class AddNoteActivity extends AppCompatActivity implements AddNoteFragment.OnFragmentInteractionListener {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    @Override
    public void onFragmentInteraction(Uri uri) {
        
    }
}
