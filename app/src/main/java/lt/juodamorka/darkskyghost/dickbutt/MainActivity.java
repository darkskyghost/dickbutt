package lt.juodamorka.darkskyghost.dickbutt;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity implements AddNoteFragment.OnFragmentInteractionListener, NoteListFragment.OnListFragmentInteractionListener {
    
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMainActivity);
        setSupportActionBar(toolbar);
    
        final NoteListFragment noteListFragment = new NoteListFragment();
        noteListFragment.setArguments(getIntent().getExtras());
    
        final AddNoteFragment addNoteFragment = new AddNoteFragment();
        addNoteFragment.setArguments(getIntent().getExtras());
        
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (findViewById(R.id.content_main) != null) {
                    if (savedInstanceState != null) {
                        return;
                    }
                    Intent intent = new Intent(getBaseContext(), AddNoteActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onFragmentInteraction(Uri uri) {
    
    }
    
    @Override
    public void onListFragmentInteraction(Note item) {
        
    }
}
